public class StringFunctions {

    public static String concat(String a, String b) {
        StringBuilder builder = new StringBuilder();
        builder.append(a);
        builder.append(b);
        return builder.toString();
    }

    public static String minus(String a, String b) {
        StringBuilder builder = new StringBuilder(a);
        for (char letter : b.toCharArray()) {
            for (int i = 0; i < builder.length(); ++i) {
                if (builder.charAt(i) == letter) {
                    builder.deleteCharAt(i);
                }
            }
        }
        return builder.toString();
    }

    public static String doublePlus(String str) {
        return concat(str, str);
    }

    public static String expression(String expr) {
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < expr.length(); ++i) {
            if(expr.charAt(i) == '+') {
                if(i == expr.length() - 1) {
                    throw new  IllegalArgumentException("Invalid operand");
                }
                if(expr.charAt(i+1) == '+') {
                    result.append(result);
                    i = i+1;
                }
            } else if(expr.charAt(i) == '-') {
                StringBuilder temp = new StringBuilder();
                int j = i + 1;
                for(; j < expr.length(); j++) {
                    if(expr.charAt(j) != '+' && expr.charAt(j) != '-') {
                        temp.append(expr.charAt(j));
                    } else break;
                }
                i = j + 1;
                result = new StringBuilder(minus(result.toString(), temp.toString()));
            } else {
                result.append(expr.charAt(i));   
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(expression("gosho+++++y++"));
    }

}
